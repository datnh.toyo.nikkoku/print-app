<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PrintControler extends Controller
{
    public function openApp(Request $request)
    {
        $data = $request->all();
        $param1 = $data['param1'] ?? '';
        $param2 = $data['param2'] ?? '';
        $listParam = "";
        if(!empty($param1)){
            $listParam .= $param1;
        }
        if(!empty($param2)){
            $listParam .= " " . $param2;
        }
        
        $appPath = "..\\appTest\\TNV_Dat.exe";
        $existFile = file_exists($appPath);
        if($existFile){
            popen("start $appPath $listParam", "r");
            return response()->json([], 200);
        }else{
            return response()->json([], 400);
        }
    }
}
